build version:
  ./build 
  ./build --diaspora-version {{version}}

scout version:
  docker scout cves --only-severity critical registry://koehn/diaspora:{{version}}
